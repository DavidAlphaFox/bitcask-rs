use crate::error::{BitCaskError, Result};
use serde::{Deserialize, Serialize};
use std::fs;
use std::fs::{File, FileType};
use tokio::fs::OpenOptions;

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub max_data_file_size: u64,
    pub max_key_size: u32,
    pub max_value_size: u64,
    pub sync: bool,
    pub auto_recovery: bool,
    pub db_version: u32,
    pub check_sum_at_get_key: bool,
    // #[serde(skip_serializing)]
    // dir_file_mode_before_umask: OpenOptions,
    // #[serde(skip_serializing)]
    // file_file_mode_before_umask: OpenOptions,
}

impl Config {
    pub fn save(&self, path: &str) -> Result<()> {
        fs::write(path, serde_json::to_vec(&self).unwrap())
            .map_err(|err| BitCaskError::FailedOpenDB(err.to_string()))
    }
    pub fn load(path: &str) -> Result<Self> {
        let buf = fs::read_to_string(path)?;
        serde_json::from_str(&*buf).map_err(|err| BitCaskError::FailedOpenDB(err.to_string()))
    }

    pub fn set_max_data_file_size(mut self, size: u64) -> Self {
        self.max_data_file_size = size;
        self
    }

    pub fn set_max_key_size(mut self, size: u32) -> Self {
        self.max_key_size = size;
        self
    }

    pub fn set_max_value_size(mut self, size: u64) -> Self {
        self.max_value_size = size;
        self
    }

    pub fn auto_sync(mut self, auto_sync: bool) -> Self {
        self.sync = auto_sync;
        self
    }

    pub fn set_auto_recover(mut self, auto_recover: bool) -> Self {
        self.auto_recovery = auto_recover;
        self
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            max_data_file_size: u64::MAX,
            max_key_size: u32::MAX,
            max_value_size: u64::MAX,
            sync: true,
            auto_recovery: true,
            db_version: 1,
            check_sum_at_get_key: false,
        }
    }
}
