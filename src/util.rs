use crate::error::Result;
use std::path::Path;

pub(crate) fn get_data_files(path: &str) -> Result<Vec<String>> {
    let mut ps = Path::new(path)
        .iter()
        .filter(|file| file.to_str().unwrap().ends_with(".data"))
        .map(|file| file.to_string_lossy().to_string())
        .collect::<Vec<_>>();
    ps.sort();
    Ok(ps)
}
