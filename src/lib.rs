#![feature(with_options)]

mod codec;
mod config;
mod data_file;
mod entry;
mod error;
mod item;
mod metadata;
mod radix_tree;
mod recover;
mod util;

use crate::codec::KeyValue;
use crate::config::Config;
use crate::data_file::DataFile;
use crate::entry::{Entry, CRC32};
use crate::error::BitCaskError::{
    EmptyKey, ExpiredKey, TooLargeKey, TooLargeValue, UnexpectedError,
};
use crate::error::Result;
use crate::item::Hint;
use crate::metadata::MetaData;
use crate::radix_tree::{Index, Indexer};
use log::{debug, error};
use std::collections::HashMap;
use std::fs;
use std::fs::{rename, File};
use std::path::Path;
use std::sync::{Arc, Mutex, MutexGuard};

pub struct BitCask {
    inner: Arc<Mutex<BitCaskCore>>,
}

pub struct Opts {}

pub struct Stats {
    data_files: u64,
    keys: u64,
    value: u64,
}

impl Default for Opts {
    fn default() -> Self {
        Opts {}
    }
}

impl BitCask {
    const LOCK_FILE: &'static str = "lock";
    const TTL_INDEX_FILE: &'static str = "ttl_index";

    pub fn open(path: &Path, cfg: Config) -> Self {
        let path = vec![path.to_str().unwrap(), "bitcask"].join("/");
        match fs::create_dir_all(path) {
            Ok(_) => {}
            Err(err) if err.kind() == ::std::io::ErrorKind::AlreadyExists => {}
            Err(err) => panic!("{}", err),
        }

        let index = Indexer::new();
        let ttl = Indexer::new();
        BitCask {
            inner: Arc::new(Mutex::new(BitCaskCore::new(index, ttl))),
        }
    }

    fn load_indexes(&mut self, data_file: HashMap<isize, DataFile>, last_index: isize) {}

    pub fn put(&self, key: Vec<u8>, value: Vec<u8>) -> Result<()> {
        if key.is_empty() {
            return Err(EmptyKey);
        }
        let mut bc = self.lc();
        if bc.config.max_key_size > 0 && bc.config.max_key_size < key.len() as u32 {
            return Err(TooLargeKey);
        }
        if bc.config.max_value_size > 0 && bc.config.max_value_size < value.len() as u64 {
            return Err(TooLargeValue);
        }
        let entry_sz = bc.put(key.clone(), value, None)?;
        if bc.config.sync {
            bc.curr.as_mut().unwrap().sync()?;
        }
        // in case of successful `put`, index_up_to_date will be always be false.
        bc.metadata.index_up_to_date = false;
        if let Some(old_item) = bc.hint_index.get(&key) {
            bc.metadata.reclaimable_space += Hint::SIZE as i64;
        }
        let item = Hint {
            file_id: bc.curr.as_ref().unwrap().file_id(),
            offset: bc.curr.as_ref().unwrap().offset,
            size: entry_sz as u64,
        };
        bc.hint_index.insert(key, item);
        Ok(())
    }

    pub fn get(&self, key: &Vec<u8>) -> Option<Vec<u8>> {
        let bc = self.lc();
        bc.get(key)
    }

    pub fn exists(&self, key: &Vec<u8>) -> bool {
        let bc = self.lc();
        if let Some(index) = bc.hint_index.get(key) {
            return !bc.expired(key);
        }
        return false;
    }
    fn lc(&self) -> MutexGuard<'_, BitCaskCore> {
        self.inner.lock().unwrap()
    }
}

struct BitCaskCore {
    hint_index: Indexer<Vec<u8>, Hint>,
    ttl_index: Indexer<Vec<u8>, Entry>,
    curr: Option<DataFile>,
    datafiles: HashMap<i64, DataFile>,
    config: Config,
    path: String,
    metadata: MetaData,
}

impl BitCaskCore {
    pub fn new(hint: Indexer<Vec<u8>, Hint>, ttl_index: Indexer<Vec<u8>, Entry>) -> Self {
        BitCaskCore {
            hint_index: hint,
            ttl_index,
            curr: None,
            datafiles: Default::default(),
            config: Default::default(),
            path: "".to_string(),
            metadata: Default::default(),
        }
    }

    fn get(&self, key: &Vec<u8>) -> Option<Vec<u8>> {
        let check_sum_at_get_key = self.config.check_sum_at_get_key;
        let mut hint;
        {
            let index = self.hint_index.get(key);
            if index.is_none() || self.expired(key) {
                return None;
            }
            hint = index.unwrap().clone();
        }

        let mut data_file = {
            if let Some(curr) = self.curr.as_ref() {
                if curr.id == hint.file_id {
                    curr
                } else {
                    self.datafiles.get(&hint.file_id).unwrap()
                }
            } else {
                self.datafiles.get(&hint.file_id).unwrap()
            }
        };
        let entry = data_file.read_at(hint.offset, hint.size as usize).unwrap();
        if check_sum_at_get_key && entry.check_sum != CRC32.checksum(&*entry.value) {
            error!(
                "file_id: {}, key: {:?}, failed to checksum",
                data_file.id, key
            );
            panic!();
        }
        Some(entry.value)
    }

    fn put(
        &mut self,
        key: Vec<u8>,
        value: Vec<u8>,
        expiry: impl Into<Option<u32>>,
    ) -> Result<usize> {
        self.maybe_rotate()?;
        let mut cur = self.curr.as_mut().unwrap();
        let entry = Entry {
            check_sum: 0,
            key,
            offset: 0,
            value,
            expiry: expiry.into().unwrap_or_else(|| 0),
        };
        cur.write(&entry)?;
        Ok(entry.size())
    }

    fn expired(&self, key: &Vec<u8>) -> bool {
        if let Some(entry) = self.ttl_index.get(key) {
            return entry.expiry > 0 && entry.expiry <= chrono::Utc::now().timestamp() as u32;
        }
        return false;
    }

    fn maybe_rotate(&mut self) -> Result<()> {
        // close the file descriptors
        let mut curr = self.curr.take();
        let mut curr = curr.unwrap();
        if curr.size() < self.config.max_data_file_size {
            return Ok(());
        }
        let id = curr.file_id();
        curr.move_to_read(&*self.data_file_name(id))?;
        debug!("move to read {}", id);
        self.datafiles.insert(id, curr);
        // create a new only write datafile
        {
            let wt_fd = File::with_options()
                .write(true)
                .read(true)
                .create(true)
                .open(self.data_file_name(id + 1))?;
            let new_data_file = DataFile {
                id: id + 1,
                fs: Some(wt_fd),
                file_name: self.data_file_name(id + 1),
                offset: 0,
            };
            debug!("create a new data file: {}", new_data_file.file_name);
            self.curr = Some(new_data_file);
        }

        // save indexes
        {
            self.save_indexes()?;
        }
        Ok(())
    }

    fn data_file_name(&self, id: i64) -> String {
        let sub_fix = format!("{}.data", id);
        let path = Path::new(self.path.as_str()).join(sub_fix.as_str());
        path.to_str().unwrap().to_string()
    }

    fn save_indexes(&mut self) -> Result<()> {
        // rename new index file
        let data_file_name = "temp_index";
        let path = Path::new(self.path.as_str()).join(data_file_name);
        let temp_index_file = path.as_os_str().to_string_lossy();
        let temp_index_file = temp_index_file.as_ref();
        {
            self.hint_index.save(temp_index_file)?;
            debug!("save temp index: {}", temp_index_file);
            rename(temp_index_file, path.join("index"))
                .map_err(|err| UnexpectedError(err.to_string()))?;
            debug!(
                "replace a new index file: {}",
                path.join("index").as_os_str().to_string_lossy()
            );
        }
        // rename new ttl index
        {
            self.ttl_index.save(temp_index_file)?;
            debug!("save temp index: {}", temp_index_file);
            rename(temp_index_file, path.join("ttl_index"))
                .map_err(|err| UnexpectedError(err.to_string()))?;
            debug!(
                "replace a new index file: {}",
                path.join("ttl_index").as_os_str().to_string_lossy()
            );
        }
        Ok(())
    }
}

impl Drop for BitCaskCore {
    fn drop(&mut self) {
        if let Err(err) = self.save_indexes() {
            error!("failed to save indexes, err: {}", err);
        }
        self.metadata.index_up_to_date = true;
        if let Err(err) = self
            .metadata
            .save(Path::new(&self.path).join("metadata.toml"))
        {
            error!("failed to save metadata: err: {}", err);
        }
    }
}

#[cfg(test)]
mod tests {
    // use crate::{BitCask, Opts};
    use std::path::Path;

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
        // BitCask::open(Path::new("/tmp"), Opts::default());
    }
}
