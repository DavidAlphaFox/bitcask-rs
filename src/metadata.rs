use crate::error::Result;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Bytes;
use std::path::Path;
use std::ptr::write_bytes;

#[derive(Default, Serialize, Deserialize)]
pub struct MetaData {
    pub(crate) index_up_to_date: bool,
    pub(crate) reclaimable_space: i64,
}

impl MetaData {
    pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let buf = toml::to_vec(&self).unwrap();
        ::std::fs::write(path, buf).map_err(|err| err.into())
    }

    pub fn load(path: &str) -> Self {
        let buf = ::std::fs::read_to_string(path).unwrap();
        toml::from_str(&*buf).unwrap()
    }

    pub fn load_and_create(path: &str) -> Self {
        let p = Path::new(path).join("meta.toml");
        if !p.exists() {
            return MetaData {
                index_up_to_date: false,
                reclaimable_space: 0,
            };
        }
        Self::load(path)
    }
}
