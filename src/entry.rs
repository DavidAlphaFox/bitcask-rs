use crate::codec::{Decode, Encode, KeyValue};
use crate::error::BitCaskError::{NoMoreData, UnexpectedError};
use crate::error::{BitCaskError, Result};
use byteorder::{ReadBytesExt, WriteBytesExt};
use crc::{Crc, CRC_32_CKSUM};
use serde::{Deserialize, Serialize};
use std::cell::{RefCell, RefMut};
use std::fs::File;
use std::io::SeekFrom::Current;
use std::io::{Cursor, Read, Write};
use std::str::Bytes;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

// key_sz: value_sz: ttl: key: value: crc
#[derive(Debug, Default, Serialize, Deserialize)]
pub(crate) struct Entry {
    pub(crate) check_sum: u32,
    pub(crate) key: Vec<u8>,
    // offset need to serde
    pub(crate) offset: u32,
    pub(crate) value: Vec<u8>,
    // TODO: zip expiry
    pub(crate) expiry: u32,
}

pub const CRC32: Crc<u32> = Crc::<u32>::new(&CRC_32_CKSUM);

impl Entry {
    const KEY_SIZE: u32 = 4;
    const VALUE_SIZE: u64 = 8;
    const CHECKSUM_SIZE: u32 = 4;
    const TTL_SIZE: u64 = 8;
    const MATA_INFO_SIZE: usize = Self::KEY_SIZE as usize
        + Self::VALUE_SIZE as usize
        + Self::CHECKSUM_SIZE as usize
        + Self::TTL_SIZE as usize;

    pub(crate) fn new(key: Vec<u8>, value: Vec<u8>, expiry: u32) -> Entry {
        let check_sum = CRC32.checksum(&value);
        Self {
            check_sum,
            key,
            value,
            expiry,
            offset: 0,
        }
    }

    pub(crate) fn size(&self) -> usize {
        return Self::MATA_INFO_SIZE + self.key.len() + self.value.len();
    }

    pub(crate) fn from_vec(buf: Vec<u8>) -> Result<Entry> {
        let mut fs = Cursor::new(buf);
        let entry = Entry::decode(&mut fs)?;
        Ok(entry)
    }

    pub(crate) fn to_vec(&self) -> Result<Vec<u8>> {
        let mut fs = Cursor::new(vec![]);
        self.encode(&mut fs).unwrap();
        Ok(Vec::from(fs.into_inner()))
    }
}

impl From<Vec<u8>> for Entry {
    fn from(_: Vec<u8>) -> Self {
        let mut entry = Entry {
            check_sum: 0,
            key: vec![],
            offset: 0,
            value: vec![],
            expiry: 0,
        };
        entry.to_vec().unwrap();
        entry
    }
}

impl Into<Vec<u8>> for Entry {
    fn into(self) -> Vec<u8> {
        let size = self.size();
        let mut cursor = Cursor::new(vec![0; size]);
        self.encode(&mut cursor).unwrap();
        cursor.into_inner()
    }
}

impl Encode for Entry {
    fn encode<Wt: Write>(&self, fs: &mut Wt) -> Result<()> {
        use byteorder::{BigEndian, ReadBytesExt};
        use bytes::Bytes;
        fs.write_u32::<BigEndian>(self.check_sum)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.write_u32::<BigEndian>(self.expiry as u32)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.write_u32::<BigEndian>(self.key.len() as u32)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.write_u64::<BigEndian>(self.value.len() as u64)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.write(&*self.key)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.write(&*self.value)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.flush()
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        Ok(())
    }
}

impl Decode<Entry> for Entry {
    fn decode<Rd: Read>(fs: &mut Rd) -> Result<Entry> {
        use byteorder::{BigEndian, ReadBytesExt};
        use bytes::Bytes;
        let mut entry = Entry::default();
        entry.check_sum = match fs.read_u32::<BigEndian>() {
            Ok(check_sum) => check_sum,
            Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => return Err(NoMoreData),
            Err(err) => return Err(UnexpectedError(err.to_string())),
        };
        entry.expiry = fs
            .read_u32::<BigEndian>()
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        let key_sz = fs
            .read_u32::<BigEndian>()
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        let value_sz: u64 = fs
            .read_u64::<BigEndian>()
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;

        entry
            .key
            .extend(::std::iter::repeat(0).take(key_sz as usize));
        entry
            .value
            .extend(::std::iter::repeat(0).take(value_sz as usize));
        fs.read(&mut entry.key)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        fs.read(&mut entry.value)
            .map_err(|err| BitCaskError::UnexpectedError(err.to_string()))?;
        Ok(entry)
    }
}

impl KeyValue<Vec<u8>, Entry> for Entry {
    #[inline]
    fn key_value(self) -> (Vec<u8>, Entry) {
        let key = self.key.clone();
        (key, self)
    }
}
