use crate::codec::{Decode, Encode, KeyValue};
use crate::entry::Entry;
use crate::error::BitCaskError::NoMoreData;
use crate::error::{is_io_eof, is_no_more_data, BitCaskError::UnexpectedError, Result};
use crate::item::Hint;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use qp_trie::Trie;
use serde::de::Unexpected::Bytes;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, BufWriter, Cursor, Read, Seek, SeekFrom, Write};
use std::ops::Deref;
use std::sync::{Arc, Mutex};

// TODO add async read/write

pub(crate) trait Index<K, V> {
    fn save(&mut self, path: &str) -> Result<bool>;
    fn load(&mut self, path: &str) -> Result<()>;
    fn get(&self, key: &Vec<u8>) -> Option<&V>;
    fn get_mut(&mut self, key: &Vec<u8>) -> Option<&mut V>;
    fn insert(&mut self, key: K, value: V) -> Option<V>;
}

pub(crate) struct Indexer<K: Borrow<[u8]>, V: Encode + Decode<V>> {
    store: Trie<K, V>,
}

impl<K, V> Indexer<K, V>
where
    K: Borrow<[u8]>,
    V: Encode + Decode<V> + From<Vec<u8>> + KeyValue<K, V>,
{
    pub(crate) fn new() -> Indexer<K, V> {
        Indexer { store: Trie::new() }
    }
}

impl<K, V> Index<K, V> for Indexer<K, V>
where
    K: Borrow<[u8]>,
    V: Encode + Decode<V> + From<Vec<u8>> + KeyValue<K, V>,
{
    fn save(&mut self, path: &str) -> Result<bool> {
        match File::with_options().write(true).create(true).open(path) {
            Ok(mut fp) => {
                for (key, entry) in self.store.iter_mut() {
                    entry.encode(&mut fp)?;
                }
                fp.flush().map_err(|err| UnexpectedError(err.to_string()))?;
                return Ok(true);
            }
            Err(err) => Err(UnexpectedError(err.to_string())),
        }
    }

    fn load(&mut self, path: &str) -> Result<()> {
        match File::with_options().read(true).open(path) {
            Ok(mut fp) => {
                loop {
                    match V::decode(&mut fp) {
                        Ok(entry) => {
                            let (k, v) = entry.key_value();
                            self.store.insert(k, v);
                        }
                        Err(ref err) if is_no_more_data(err) => {
                            break;
                        }
                        Err(err) => return Err(err),
                    }
                }
                Ok(())
            }
            Err(err) => Err(UnexpectedError(err.to_string())),
        }
    }

    fn get(&self, key: &Vec<u8>) -> Option<&V> {
        self.store.get(key)
    }

    fn get_mut(&mut self, key: &Vec<u8>) -> Option<&mut V> {
        self.store.get_mut(key)
    }

    fn insert(&mut self, key: K, value: V) -> Option<V> {
        self.store.insert(key, value)
    }
}

pub struct IIndex {
    store: Trie<Vec<u8>, Hint>,
}

#[test]
fn it_works() {}

#[test]
fn radix_tree() {
    use names::{Generator, Name};
    use tempdir::TempDir;

    let dir = TempDir::new("my_directory_prefix").unwrap();
    let file_path = dir.path().join("trie.index.txt");
    println!("{:?}", file_path);
    let mut tree = Indexer::new();
    let mut name_g = Generator::with_naming(Name::Numbered);
    for i in 0..10 {
        let key = name_g.next().unwrap().into_bytes();
        let value = name_g.next().unwrap().into_bytes();
        tree.insert(key.clone(), Entry::new(key, value, i));
    }

    let ok = tree.save(file_path.to_str().unwrap()).unwrap();
    assert!(ok);

    drop(tree);
    let mut tree: Indexer<Vec<u8>, Entry> = Indexer::new();
    let got = tree.load(file_path.to_str().unwrap());
    assert!(got.is_ok());

    tree.store.iter().for_each(|(key, value)| {
        assert_eq!(key, &value.key);
        println!(
            "{}, {:?}",
            String::from_utf8_lossy(key),
            serde_json::to_string(value).unwrap(),
        )
    })
}
