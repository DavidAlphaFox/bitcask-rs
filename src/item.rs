use crate::codec::{Decode, Encode, KeyValue};
use crate::error::BitCaskError::{NoMoreData, UnexpectedError};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use serde::{Deserialize, Serialize};
use std::io::{Cursor, Read, Write};

// `Item` represents the location of the value on disk. This is used by the
// internal Adaptive Radix Tree to hold an in-memory structure mapping keys to
// locations on disk of where the value(s) can be read from.(Only one Hint, not same to raw bitcask)
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub(crate) struct Hint {
    pub(crate) file_id: i64,
    pub(crate) offset: u64,
    pub(crate) size: u64,
}

impl Hint {
    pub(crate) const SIZE: u64 = 8 + 8 + 8;
}

impl From<Vec<u8>> for Hint {
    fn from(v: Vec<u8>) -> Self {
        let mut rd = Cursor::new(v);
        Self::decode(&mut rd).unwrap()
    }
}

impl KeyValue<Vec<u8>, Hint> for Hint {
    #[inline]
    fn key_value(self) -> (Vec<u8>, Hint) {
        let mut cursor = Cursor::new(Vec::with_capacity(4));
        cursor.write_i64::<BigEndian>(self.file_id).unwrap();
        (cursor.into_inner(), self)
    }
}

impl Decode<Hint> for Hint {
    fn decode<Rd: Read>(fs: &mut Rd) -> crate::error::Result<Hint> {
        let mut item = Hint::default();
        item.file_id = {
            match fs.read_i64::<BigEndian>() {
                Ok(file_id) => file_id,
                Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => {
                    return Err(NoMoreData)
                }
                Err(err) => return Err(UnexpectedError(err.to_string())),
            }
        };
        item.offset = fs
            .read_u64::<BigEndian>()
            .map_err(|err| UnexpectedError(err.to_string()))?;
        item.size = fs
            .read_u64::<BigEndian>()
            .map_err(|err| UnexpectedError(err.to_string()))?;
        Ok(item)
    }
}

impl Encode for Hint {
    fn encode<Wt: Write>(&self, fs: &mut Wt) -> crate::error::Result<()> {
        let mut cursor = Cursor::new(Vec::with_capacity(Self::SIZE as usize));
        cursor
            .write_i64::<BigEndian>(self.file_id)
            .map_err(|err| UnexpectedError(err.to_string()))?;
        cursor
            .write_u64::<BigEndian>(self.offset)
            .map_err(|err| UnexpectedError(err.to_string()))?;
        cursor
            .write_u64::<BigEndian>(self.size)
            .map_err(|err| UnexpectedError(err.to_string()))
    }
}
