use crate::codec::{Decode, Encode, KeyValue};
use crate::config;
use crate::entry::Entry;
use crate::error::BitCaskError::UnexpectedError;
use crate::error::{is_corrupted_data, is_io_eof, BitCaskError, Result};
use log::{debug, warn};
use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::fs::{remove_file, rename, File};
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::Path;

#[derive(Default)]
pub(crate) struct DataFile {
    pub(crate) id: i64,
    pub(crate) fs: Option<File>,
    pub(crate) file_name: String,
    pub(crate) offset: u64,
}

impl DataFile {
    pub(crate) fn new(id: i64, fs_name: String, only_read: bool) -> DataFile {
        let mut write = true;
        if only_read {
            write = false;
        }
        let fs = File::with_options()
            .write(write)
            .read(true)
            .open(fs_name.as_str())
            .unwrap();
        DataFile {
            id,
            fs: Some(fs),
            file_name: "".to_string(),
            offset: 0,
        }
    }

    pub(crate) fn file_id(&self) -> i64 {
        self.id
    }

    fn name(&self) -> &String {
        &self.file_name
    }

    pub(crate) fn sync(&mut self) -> Result<()> {
        let fs = self.fs.as_ref().unwrap();
        fs.sync_all().map_err(|err| err.into())
    }

    pub(crate) fn size(&self) -> u64 {
        let fs = self.fs.as_ref().unwrap();
        fs.metadata().unwrap().len()
    }

    pub(crate) fn move_to_read(&mut self, file_name: &str) -> Result<()> {
        {
            self.fs.take();
        }
        let fs = File::with_options()
            .read(true)
            .open(file_name)
            .map_err(|err| UnexpectedError(err.to_string()))?;
        self.fs = Some(fs);
        self.offset = 0;
        Ok(())
    }

    fn read(&self) -> Result<Entry> {
        let mut fs = self.fs.as_ref().unwrap();
        let mut entry = Entry::decode(&mut fs)?;
        Ok(entry)
    }

    pub(crate) fn read_at(&self, index: u64, rd_size: usize) -> Result<Entry> {
        let mut fs = self.fs.as_ref().unwrap();
        fs.seek(SeekFrom::Start(index))
            .map_err(|err| UnexpectedError(err.to_string()))?;
        let mut buf = vec![0; rd_size];
        fs.read(&mut buf)
            .map_err(|err| UnexpectedError(err.to_string()))?;
        Entry::from_vec(buf)
    }

    pub(crate) fn write(&mut self, entry: &Entry) -> Result<()> {
        let fs = self.fs.as_mut().unwrap();
        entry.encode(fs)?;
        *self.offset.borrow_mut() += entry.size() as u64;
        Ok(())
    }
}

pub(crate) fn recover_data_file(path: &str, cfg: &config::Config) -> Result<bool> {
    let mut fs_rd = File::open(path).map_err(|err| UnexpectedError(err.to_string()))?;

    let p = Path::new(path);
    let file_name = p.file_name().unwrap().to_string_lossy().to_string();
    let recover_file_path = p.with_file_name(format!("{}.recover", file_name));
    let mut fs_wd = File::with_options()
        .create(true)
        .write(true)
        .append(true)
        .open(recover_file_path.clone())
        .map_err(|err| UnexpectedError(err.to_string()))?;
    let mut corrupted = false;
    while corrupted {
        match Entry::decode(&mut fs_rd) {
            Ok(entry) => {}
            Err(err) if is_corrupted_data(&err) => {
                // TODO it should be no happen
                warn!("{} is corrupted, a best-effort recovery was done", path);
                corrupted = true;
                continue;
            }
            Err(err) if is_io_eof(&err) => break,
            Err(err) => return Err(err),
        }
    }
    drop(fs_wd);
    if !corrupted {
        remove_file(&recover_file_path).map_err(|err| UnexpectedError(err.to_string()))?;
        debug!("{} is not corrupted, remove it", path);
        return Ok(false);
    }
    drop(fs_rd);
    // cover `path` file
    rename(&recover_file_path, path).map_err(|err| UnexpectedError(err.to_string()))?;
    debug!("cover file '{}' had renamed at recover", file_name);
    Ok(true)
}

// pub(crate) fn recover_data_file(path: &str, cfg: &config::Config, dry_run: bool) -> Result<()> {
//     let mut fs_rd = RefCell::new(File::open(path).map_err(|err| UnexpectedError(err.to_string()))?);
//
//     let p = Path::new(path);
//     let file_name = p.file_name().unwrap().to_string_lossy().to_string();
//     let recover_file_path = p.with_file_name(format!("{}.recover", file_name));
//     let mut fs_wd = RefCell::new(
//         File::with_options()
//             .create(true)
//             .write(true)
//             .append(true)
//             .open(recover_file_path)
//             .map_err(|err| UnexpectedError(err.to_string()))?,
//     );
//     loop {
//         let mut entry = Entry::default();
//         match entry.decode(fs_rd.borrow_mut()) {
//             Ok(_) => {
//                 if dry_run {
//                     continue;
//                 }
//                 entry.encode(fs_wd.borrow_mut())?;
//             }
//             Err(err) if is_corrupted_data(&err) => {
//                 warn!("{} is corrupted, a best-effort recovery was done", path);
//                 return Ok(());
//             }
//             Err(err) if is_io_eof(&err) => break,
//             err => return err,
//         }
//     }
//     debug!("{} is not corrupted", path);
//     Ok(())
// }

#[test]
fn data_file_write_and_read() {
    use serde_json::to_string;
    use tempdir::TempDir;
    let dir = TempDir::new("my_directory_prefix").unwrap();
    let file_path = dir.path().join("foo.txt");
    println!("{:?}", file_path);
    let mut fs = File::with_options()
        .read(true)
        .write(true)
        .append(true)
        .create(true)
        .open(file_path)
        .unwrap();

    for i in 0..10 {
        let mut entry = Entry::new(
            format!("{}", i).as_bytes().to_vec(),
            format!("{}", i).as_bytes().to_vec(),
            100,
        );
        assert!(entry.encode(&mut fs).is_ok())
    }
    assert!(fs.seek(SeekFrom::Start(0)).is_ok());

    for i in 0..10 {
        let entry = Entry::decode(&mut fs);
        assert!(entry.is_ok());
        println!("{:?}", to_string(&entry.unwrap()).unwrap());
    }
}

#[test]
fn data_file_core_samples() {}
