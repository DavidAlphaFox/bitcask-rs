use crate::codec::{Decode, Encode};
use crate::config;
use crate::data_file;
use crate::entry::Entry;
use crate::error::BitCaskError::UnexpectedError;
use crate::error::{is_corrupted_data, is_io_eof, BitCaskError, Result};
use crate::util::get_data_files;
use log::{debug, warn};
use std::cell::RefCell;
use std::fs::{remove_file, File};
use std::path::Path;

// Checks and recovers the last datafile.
// If the datafile isn't corrupted, this is a noop. If it is,
// the longest non-corrupted prefix will be kept and the reset
// will be *deleted*. Also, the index file is also *deleted* which
// will be automatically recreated on next startup.
pub fn check_and_recover(path: &str, cfg: &config::Config) -> Result<()> {
    let dfs = get_data_files(path)?;
    if dfs.is_empty() {
        debug!("not found any data files");
        return Ok(());
    }
    // recover the last file.
    let f = dfs.last().unwrap();
    let recovered = data_file::recover_data_file(f, cfg)?;
    if recovered {
        remove_file(Path::new(path).join("index"))
            .map_err(|err| UnexpectedError(err.to_string()))?;
        debug!(
            "{} index file had removed",
            Path::new(path).join("index").to_str().unwrap()
        );
    }
    Ok(())
}

pub(crate) fn recover(path: &str, dry_run: bool) -> Result<usize> {
    use crate::config::Config;
    let config = Config::load(
        Path::new(path)
            .join("config.json")
            .as_os_str()
            .to_str()
            .unwrap(),
    )?;
    todo!()
}

fn recover_index(path: &str, max_key_size: u32, dry_run: bool) -> Result<()> {
    todo!()
}
